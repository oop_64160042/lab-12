package com.chanisata;

import java.util.ArrayList;
import java.util.Collections;

public class TestArrayList {
    public static void main(String[] args) {
        System.out.println("---------List 1---------");
        ArrayList<String> list = new ArrayList<String>();
        list.add("A1");
        list.add("A2");
        list.add("A3");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println();

        list.add(1, "A0");
        list.add("A3");
        for (String str : list) {
            System.out.println(str);
        }
        System.out.println();

        String[] arr = list.toArray(new String[list.size()]);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
        System.out.println();

        // remove
        System.out.println("---------remove---------");
        list.remove("A0");
        System.out.println(list);
        list.remove(3);
        System.out.println(list);

        // set
        System.out.println("---------set---------");
        list.set(1, "A0");
        System.out.println("Set " + list);

        System.out.println("---------List 2---------");
        ArrayList<String> list2 = new ArrayList(list);
        System.out.println(list2);
        list2.add("A4");
        System.out.println(list2);

        System.out.println("---------addAll---------");
        System.out.println("List 1 + List 2");
        list.addAll(list2);
        System.out.println(list);

        System.out.println("---------contains()---------");
        System.out.println(list2.contains("A0"));
        System.out.println(list2.contains("A10"));

        System.out.println("---------indexOf()---------");
        System.out.println(list2.indexOf("A0"));
        System.out.println(list2.indexOf("A10"));

        System.out.println("---------Collections---------");
        Collections.sort(list);
        System.out.println("sort" + list);
        Collections.shuffle(list);
        System.out.println("shuffle" + list);
        Collections.reverse(list);
        System.out.println("reverse" +list);
        Collections.swap(list, 0, 1);
        System.out.println("swap" +list);
    }
}
